Read and write Sobek HIS files.

## Getting started
In absence of actual documentation, here are some of the functions:
```python
import his

pn = his.read('results.his')
his.write('results-out.his', pn)

# The idea is that once you have the data in a pandas Panel,
# it is easy to convert to other formats, or directly do
# plotting/calculation/aggregation/modification.
pn.to_csv('results.csv')
```

Since pandas Panel is deprecated, the plan is to switch it over and return
a xarray Dataset instead.

If you need to read the similar-but-different MPX file format, use
`his.mpx.read("file.mpx")` instead.
